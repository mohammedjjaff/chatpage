import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  List<MessageModel> message = [
    MessageModel(message: 'اكيد', sendId: 1),
    MessageModel(message: '🇵🇸 فلسطين حره', sendId: 0),
    MessageModel(message: ' .. نسيت شغله', sendId: 0),
    MessageModel(message: 'كيف يعمل بالضبط هذا الكود', sendId: 0),
    MessageModel(message: 'مرحبا', sendId: 0),
  ];



  TextEditingController messageController = TextEditingController();
  bool load = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: Colors.white,
        leading: const Icon(Icons.chevron_left,color: Colors.black,),
        title: Text('دردشة مع',style: GoogleFonts.almarai(color: Colors.black,fontWeight: FontWeight.bold),),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 20),
        child: SizedBox(
            child: TextFormField(
              keyboardType: TextInputType.multiline,
              textAlign: TextAlign.right,
              controller: messageController,
              maxLines: null,
              maxLength: null,
              onChanged: (v){
                setState(() {

                });
              },
              style: GoogleFonts.almarai(fontSize: 15,color: Colors.black87),
              decoration: InputDecoration(
                hintText: 'اكتب رسالتك هنا',
                hintStyle: GoogleFonts.almarai(fontSize: 15,color: Colors.black87),
                suffixIcon: const Icon(Icons.message_outlined,color: Colors.black87,),
                prefixIcon: GestureDetector(
                  onTap: ()async{
                    setState(() {
                      load = true;
                    });
                    await Future.delayed(const Duration(seconds: 1));
                    message.insert(0,MessageModel(message: messageController.text, sendId: 1));
                    setState(() {
                      load = false;
                      messageController.clear();
                    });
                  },
                  child: Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      color: Colors.pink
                    ),
                    child: load ? const Padding(
                      padding: EdgeInsets.all(15.0),
                      child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color> (Colors.white)),
                    )
                        : const Icon(Icons.turn_slight_left_rounded,color: Colors.white,size: 25,),
                  ),
                ),
                counterStyle: GoogleFonts.almarai(color: Colors.black87),
                labelStyle: GoogleFonts.almarai(color: Colors.black87),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(50),
                  borderSide:  const BorderSide(color: Colors.transparent),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(50),
                  borderSide:  const BorderSide(color: Colors.transparent),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(50),
                  borderSide: const BorderSide(color: Colors.transparent),
                ),
                fillColor: Colors.grey.withOpacity(0.2),
                filled: true,
                isDense: true,
                //  contentPadding: const EdgeInsets.only(left: 10,right: 10,top: 35),
                //alignLabelWithHint: true,
              ),
            )
        ),
      ),
      body: ListView.builder(
        itemCount: message.length,
        reverse: true,
        itemBuilder: (context,index){
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 1),
            child: Row(
              mainAxisAlignment: message[index].sendId == 1 ? MainAxisAlignment.end : MainAxisAlignment.start,
              children: [
                Card(
                  elevation: 0.5,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                        topRight: const Radius.circular(20),
                        topLeft: const Radius.circular(20),
                        bottomLeft: message[index].sendId == 0 ? const Radius.circular(0) : const Radius.circular(20),
                        bottomRight: message[index].sendId == 1 ? const Radius.circular(0) : const Radius.circular(20),
                      )
                  ),
                  shadowColor: message[index].sendId == 0 ? Colors.grey.withOpacity(0.2) : Colors.pink,
                  child: Container(
                    width: MediaQuery.of(context).size.width / 1.5,
                    padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topRight: const Radius.circular(20),
                        topLeft: const Radius.circular(20),
                        bottomLeft: message[index].sendId == 0 ? const Radius.circular(0) : const Radius.circular(20),
                        bottomRight: message[index].sendId == 1 ? const Radius.circular(0) : const Radius.circular(20),
                      ),
                      color: message[index].sendId == 0 ? Colors.grey.withOpacity(0.2) : Colors.pink
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(message[index].message,textAlign: message[index].sendId == 0 ? TextAlign.left : TextAlign.right,
                        style: GoogleFonts.almarai(fontSize: 16,color: message[index].sendId == 0 ? Colors.black : Colors.white),),
                        const SizedBox(height: 10,),
                        Padding(
                          padding: EdgeInsets.only(right: message[index].sendId == 0 ? 5 : 0),
                          child: Text('2023-12-15',textAlign: message[index].sendId == 0 ? TextAlign.left : TextAlign.right,
                            style: GoogleFonts.almarai(fontSize: 14,color: message[index].sendId == 0 ? Colors.black26 : Colors.white70),),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}



class MessageModel {
  String message;
  int sendId;
  MessageModel({required this.message,required this.sendId});
}
